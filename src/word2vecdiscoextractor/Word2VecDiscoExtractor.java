/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package word2vecdiscoextractor;

import de.linguatools.disco.CorruptConfigFileException;
import de.linguatools.disco.DISCO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author jeremie
 */
public class Word2VecDiscoExtractor {
    public static String UNIGRAM_LISTS = "/home/jeremie/Downloads/UnigramsforW2Vec.txt";
    public static String DISCO_DIR = "/home/jeremie/Downloads/enwiki-20130403-word2vec-lm-mwl-lc-sim";
    public static String OUTPUT_FILE = "/home/jeremie/Downloads/output.csv";
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        List<String> unigrams = Files
                .readAllLines(new File(UNIGRAM_LISTS).toPath())
                .stream()
                .map(unigram -> unigram.replaceAll("[^a-zA-Z0-9\\s]", "").toLowerCase())
                .collect(Collectors.toList());
             
        String discoDir = DISCO_DIR;
        String outputFile = OUTPUT_FILE;
        List<String> output = new ArrayList<>();

        DISCO discoRAM;
        try {
            discoRAM = new DISCO(discoDir, true);      
            unigrams.stream().forEach(unigram -> {
                try {          
                    Map<String, Float> wv = discoRAM.getWordvector(unigram);
                    if (wv != null) {
                        StringBuilder line = new StringBuilder();
                        line.append(unigram + "\t");
                        for (int dimension = 1; dimension < 401; dimension++) {
                            String key = "" + dimension;
                            float value = wv.getOrDefault(key, 0f);

                            line.append(value);
                            if (dimension < 400) {
                                line.append("\t");
                            } else {
                                line.append("\n");
                            }
                        }
                        output.add(line.toString());
                    }
                    
                } catch (IOException ex) {
                    Logger.getLogger(Word2VecDiscoExtractor.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            
            Files.write(new File(outputFile).toPath(), output, Charset.defaultCharset());
            
        } catch (FileNotFoundException | CorruptConfigFileException ex) {
            System.out.println("Error creating DISCO instance: " + ex);
            return;
        }
    }
}
